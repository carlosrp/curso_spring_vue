const PI = 3.1415926;

let unaVar = 20;
let unTexto = "Que pasa!";

document.write(`<br> Se puede escribir texto y variables a la vez como ${unaVar} o como ${unTexto}`);

var suma = (x, y) => { return x + y; }

var alcuadrado = x => x ** 2;

document.write(`<br> ${suma(2, 2)}`);
document.write(`<br> ${alcuadrado(13)}`);


class Dato {
    constructor(x, y = 20) {
        this.x = x;
        this.y = y;
    }
    mostrar() {
        document.write(`<br>Las cosas del objeto son "${this.x}" y "${this.y}"`);
    }


}

class Info extends Dato {
    constructor(x, y = 50, z = 30) {
        super(x, y);
        this.z = z;
    }
    mostrar() {
        super.mostrar();
        document.write(` y la ultima (z) es ${this.z}`);
    }
}
let dato = new Dato("Primera cosa");
dato.mostrar();
let info = new Info("Otra info");
info.mostrar();