//JSON = JAVA SCRIPT OBJECT NOTATION


let formaPago = {
    "modo": "Tarjeta credito",
    "comision": 2,
    "activa": true,
    "preparacion": null,
    "clientes": ["Santander", "Sabadell", "BBVA", [1, 23, 55]],
    "configuracion": {
        "conexion": "ssl",
        "latencia": 15,
    }
};

let arrayVacio = [];
let datos = ["Churros", "Merinas", 200, true, null, { "ale": "nas datos" }];
let matriz = [
    [4, 3, 2],
    [3, 2, 3],
    [1, 2, 3]
];

formaPago.servidor = "www.visa.com";

document.write(`<br><p>${formaPago.modo} - ${formaPago.clientes[1]} - ${formaPago.clientes[1][2]} </p>`);
document.write(`<br>${matriz[1][2]}`);
document.write(`<br>${JSON.stringify(formaPago)}`);
window.localStorage.setItem("datos-forma-pago", JSON.stringify(formaPago, null, 3));

alert(window.localStorage.getItem("datos-forma-pago"));
document.write(`<br>${formaPago[prompt("Que dato quieres?")]}`);

let frutas = `[
    { "nombres": "pera","precio":20},
    { "nombres": "kiwi","precio":26},
    { "nombres": "fresa","precio":29},

]`;

let objFrutas = JSON.parse(frutas);
document.write(`<br>${objFrutas[2][2]}`);