<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ejemplo JSP</title>
</head>
<body>
<h1>Ejemplo JSP</h1>
<%! String crearOL(int v){
	String ol = "<ol>";
	int i = v;
	while (i > 0) {
		ol += "<LI>Cuenta atr�s " + i + " / 5 </li>";
		i--;
	}
	return ol + "</ol>";
}
%>
<hr>
<% Date d = new Date();
if(d.getSeconds() % 2 == 0){
	%><p style="background-color: red">Prueba de nuevo</p>
	<% } else { %>
		<p style="background-color: blue">EXITO</p>
	<%= crearOL(d.getSeconds())%>
	<hr>
	<%}%>
	<%--Este comentario no se envia porque es JSP --%>
	<!-- Este si se envia -->

</body>
</html>