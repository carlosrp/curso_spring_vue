package com.grupoica.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaServlet
 */
@WebServlet("/HolaServlet")
public class HolaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HolaServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nombre = request.getParameter("nombre");
		String html = "<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "<meta charset=\"ISO-8859-1\">\r\n"
				+ "<title>Servlets GET</title>\r\n" + "</head>\r\n" + "<body>\r\n";

		if ("".equals(nombre) || nombre == null) {
			html = html + "<h1> NECESITO UN PARAMETRO</h1>";
		} else {
			html = html + " <form action=\"./hola.do\" method=\"post\">\r\n"
					+ " 	<input type=\"number\" name=\"veces\">\r\n"
					+ " 	<input type=\"submit\" value=\"POST\">\r\n" + " </form>\r\n" + "</body>\r\n" + "</html>";
		}
		response.getWriter().append(html).append(nombre);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try(PrintWriter escritor = response.getWriter()){
			String vecesString = request.getParameter("veces");
			escritor.println("<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "<meta charset=\"ISO-8859-1\">\r\n"
					+ "<title>Servlets GET</title>\r\n" + "</head>\r\n" + "<body>\r\n");
			escritor.println("<h1>Has venido desde post</h1>");
			if(vecesString == null || "".equals(vecesString)) {
				System.out.println("Usuario no ha puesto datos");
				escritor.println("<h2>Falta numero de veces");
			}
			else{
				int veces = Integer.parseInt(vecesString);
				escritor.println("<ul>");
				for (int i = 1; i <= veces; i++) {
					escritor.println("<li>" + i + " vez</li> ");
				}
				escritor.println("</ul>");
			}
			
		}
		catch(Exception e) {
			
		}
		
	}

}
