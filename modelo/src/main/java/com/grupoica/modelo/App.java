package com.grupoica.modelo;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	DerbyDBUsuario db = new DerbyDBUsuario();
        System.out.println( "Hello World!" );
        ArrayList<Usuario> usuarios = db.listar();
        for (Usuario usuario : usuarios) {
			System.out.println("Usuario: " + usuario.getNombre());
		}
    }
}
