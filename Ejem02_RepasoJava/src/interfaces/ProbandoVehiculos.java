package interfaces;

import java.util.ArrayList;

public class ProbandoVehiculos {

	public static void probar() {
		Coche miCoche = new Coche("Kia",1500, 60.34f);
		
		Coche miCocheFines = new Coche("Hammer",3500, 63.34f);
		
		Patinete miPatinete = new Patinete(100);
		
		Vehiculo unVehiculo = miCoche;
		//unVehiculo.aceleracion();
		
		ArrayList<Motorizable> garaje = new ArrayList<>();
		//garaje.add(miCaballo);
		garaje.add(miCoche);
		garaje.add(miCocheFines);
		garaje.add(miPatinete);
		//garaje.add(new Vehiculo("No comprado",3));
		
		System.out.println("Mi garaje:");
		for (Motorizable vehiculo: garaje) {
			vehiculo.encender();
		}
		
		ArrayList<Animal> granja = new ArrayList<>();
		granja.add(new Caballo("Rocinante",1500, 60));
		granja.add(new Perro(true));
		System.out.println("Mi granja:");
		
		for (Animal animal: granja) {
			animal.alimentarse("Hamburguesa");
		}
		//for(Map.Entry<String,Animal> animal : granja.entrySet())
		//Esto es como se haria el foreach con hashmap
	}
}
