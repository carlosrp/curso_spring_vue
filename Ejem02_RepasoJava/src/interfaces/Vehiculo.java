package interfaces;

public abstract class Vehiculo {
	
	private String marca;
	private float peso;
		
	public Vehiculo(String marca, float peso) {
		super();
		this.marca = marca;
		this.peso = peso;
	}
	
	public void aceleracion() {
		System.out.println("Acelerando vehiculo "+ getClass().getSimpleName() + " "+ marca + " " + peso);
	}
	
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public float getPeso() {
		return peso;
	}
	public void setPeso(float peso) {
		this.peso = peso;
	}
	
	public abstract void desplazar();
	
}
