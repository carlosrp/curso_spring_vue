package com.grupoica.repasojava;

public class EjemploLambdas {

	interface NuestraFun {
		float operacion( float v, float w);
	}
	
	
	public static float sumar(float x, float y) {
		return x + y;
	}
	public static float restar(float x, float y) {
		return x - y;
	}
	
	public static float[] sumarArrays(float[] arr1, float[] arr2) {
		float[] arrayRes = new float[arr1.length];
		for (int i = 0; i < arrayRes.length; i++) {
			arrayRes[i] = arr1[i]+arr2[i];
		}
		return arrayRes;
	}
	public static float[] operarArrays(float[] arr1, float[] arr2, NuestraFun funCB) {
		float[] arrayRes = new float[arr1.length];
		for (int i = 0; i < arrayRes.length; i++) {
			arrayRes[i] = funCB.operacion(arr1[i],arr2[i]);
		}
		return arrayRes;
	}
	
	public static void ejecutarLambdas() {
		float[] array1 = {10,8,6,4};
		float[] array2 = {1,2,3,4};
		
		System.out.println("Suma");
		for (int i = 0; i < array2.length; i++) {
			float[] arrayres = sumarArrays(array1,array2);
			System.out.print(arrayres[i] + " ");
		}
		System.out.println();
		System.out.println("Suma pero con callback");
		for (int i = 0; i < array2.length; i++) {
			float[] arrayres = operarArrays(array1,array2, EjemploLambdas::sumar);
			System.out.print(arrayres[i] + " ");
		}
		System.out.println();
		System.out.println("Resta con callback");
		for (int i = 0; i < array2.length; i++) {
			float[] arrayres = operarArrays(array1,array2, EjemploLambdas::restar);
			System.out.print(arrayres[i] + " ");
		}
		System.out.println();
		System.out.println("Division con callback y lambda");
		for (int i = 0; i < array2.length; i++) {
			float[] arrayres = operarArrays(array1,array2, (float x, float y)->{return x/y;});
			System.out.print(arrayres[i] + " ");
		}
		
	}
}
