package com.grupoica.repasojava.modelo;

public class Loco extends Usuario {
	boolean tipoLocura = false;

	public boolean isTipoLocura() {
		return tipoLocura;
	}

	public void setTipoLocura(boolean tipoLocura) {
		this.tipoLocura = tipoLocura;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (obj instanceof Usuario) {
			Usuario usuario = (Usuario) obj;
			if (this.nombre == usuario.nombre && this.edad == usuario.edad) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
		
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.nombre + " " + this.edad + " " + this.tipoLocura;
	}
	
}