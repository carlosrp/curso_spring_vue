package com.grupoica.repasojava.modelo;

public class PruebaMemoria {
	
	public static void pruebaPasoReferencia() {
		Usuario usuario = new Usuario(30, "Juan");
		int array[] = new int[3];		
		array[0] = 10; array[1]=20; array[2] = 30;
		otraFuncion(usuario, array);
		System.out.println(usuario.nombre+ " tiene " + array[0] + " a�os segun el array");
	}
	
	private static void otraFuncion(Usuario user, int[] array) {		
		System.out.println(user.nombre+ " tiene " + array[0] + " a�os segun el array");
		user.nombre = "Pedro";
		array[0] = 123;
		System.out.println(user.nombre+ " tiene " + array[0] + " a�os segun el array");
	}
	
}