package com.grupoica.repasojava.modelo;

import java.util.ArrayList;

import com.grupoica.repasojava.EjemploHashMap;

/*
 * Clase que se encargar� de las operaciones CRUD.
 */
public class GestionUsuarios {
	
	private ArrayList<Usuario> listaUsuarios;

	public GestionUsuarios() {
		this.listaUsuarios = new ArrayList<Usuario>();
		
	}
	public void listar() {
		System.out.println("LISTA DE USUARIOS:");
		for (int i = 0; i < getListaUsuarios().size(); i++) {
			System.out.println(getListaUsuarios().get(i).getNombre() + " con edad " + getListaUsuarios().get(i).getEdad());
			
		}
	}
	public Usuario listarUsuario(String nombre) {
		for (Usuario usu : getListaUsuarios()) {
			//Usuario usu = (Usuario) usuObj;
			if (usu.getNombre().equals(nombre)) {
				System.out.println("Se ha encontrado a: " + usu.getNombre());
				return usu;
			}
		}
		return null;
	}
	
	public void a�adir(Usuario usu) {
		this.getListaUsuarios().add(usu);
		
	}
	public ArrayList<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}
	public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	
	private void modificarNombre(String nombreantiguo, String nombrenuevo) {
		Usuario usuarioencontrado = listarUsuario(nombreantiguo);
		if(usuarioencontrado != null) {
			usuarioencontrado.setNombre(nombrenuevo);
			System.out.println("Se ha cambiado correctamente, ahora se llama " + nombrenuevo);
		}
		else {
			System.out.println("No hay nadie que se llame asi");
		}
	}
	private void modificarEdad(String nombreantiguo, int edadnueva) {
		Usuario usuarioencontrado = listarUsuario(nombreantiguo);
		if(usuarioencontrado != null) {
			usuarioencontrado.setEdad(edadnueva);
			System.out.println("Se ha cambiado correctamente, ahora tiene estos a�os:  " + edadnueva);
		}
		else {
			System.out.println("No hay nadie que se llame asi");
		}
		
	}
	public void modificarAmbas(String nombreantiguo, String nombrenuevo, int edadnueva) {
		modificarEdad(nombreantiguo, edadnueva);
		modificarNombre(nombreantiguo, nombrenuevo);
	}
	public void eliminarTodo() {
		this.listaUsuarios.clear();
	}
	public void eliminarUsuario(String nombre) {
		System.out.println("Procedo a eliminar a " + nombre);
		this.listaUsuarios.remove(listarUsuario(nombre));
		System.out.println("Se ha eliminado a " + nombre);
	}
	
	public void listarEdad(int edad) {
		boolean encontrado = false;
		System.out.println("Buscamos usuarios por edad ");
		for (Usuario usu : getListaUsuarios()) {
			if (usu.getEdad()==edad) {
				System.out.println("Se ha encontrado con edad " + edad + " a: " + usu.getNombre());
				encontrado = true;
				
			}
		}
		if (!encontrado) {
			System.out.println("No se ha encontrado a nadie con esa edad");
		}
	}
	public void listarEdadRango(int edadMenor, int edadMayor) {
		boolean encontrado = false;
		System.out.println("Buscamos usuarios por rango de edad "+edadMenor + "-" + edadMayor);
		for (Usuario usu : getListaUsuarios()) {
			if (usu.getEdad()<= edadMayor && usu.getEdad()>= edadMenor) {
				System.out.println("Se ha encontrado con edad " + usu.getEdad() + " a: " + usu.getNombre());
				encontrado = true;
			}
		}
		if (!encontrado) {
			System.out.println("No se ha encontrado a nadie con esa edad");
		}
	}
	

}