package com.grupoica.repasojava.modelo;

public class Usuario {
	
	int edad;
	String nombre;
	public Usuario(int edad, String nombre) {
		this.edad = edad;
		this.nombre = nombre;
	}
	public Usuario() {
		nombre="Sin nombre";
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}