package com.grupoica.repasojava;

import java.util.HashMap;
import java.util.Scanner;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploHashMap {
	
	public static HashMap<String, Usuario> diccUsuarios;
	
	public static void probandoHashMap() {
		diccUsuarios = new HashMap<>();
		diccUsuarios.put("Luis", new Usuario(11,"Luis"));
		diccUsuarios.put("Juan", new Usuario(11,"Juan"));
		diccUsuarios.put("Pedro", new Usuario(11,"Pedro"));
		Scanner escaner = new Scanner(System.in);
		System.out.println("Introduce usuario");
		String nombre = escaner.nextLine();
		System.out.println("El usuario es "+ diccUsuarios.get(nombre));
	}
}
