package com.grupoica.repasojava;

import com.grupoica.repasojava.modelo.GestionUsuarios;
import com.grupoica.repasojava.modelo.Loco;
import com.grupoica.repasojava.modelo.PruebaMemoria;
import com.grupoica.repasojava.modelo.Usuario;

import interfaces.ProbandoVehiculos;

public class ProgramaMain {

	/*
	 * P.O.O
	 * La unidad basica de almacenamiento son los tipos primitivos y los objetos
	 * que estan basados en clases. Las clases son  el molde, plantilla, estructura que indica
	 * como seran todos los objetos instanciados a partir de ella:
	 * Sus variables miembro(campos, atributos, propiedades...) y sus metodos (funciones propias)
	 * 
	 * Herencia
	 * Polimorfismo: Capacidad de los objetos de tener la misma forma que sus ancestros.
	 * Encapsulacion: Capacidad de las clases para limitar el acceso a las variables miembro y metodos. (nivel de acceso publico, privado, protected(a nivel de herencia) o default (a nivel de paquete).
	 */
	
	public static final float PI = 3.1415926f;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		GestionUsuarios gestionUsuarios = new GestionUsuarios();
		Usuario usu = new Usuario(23, "Fulano");
		gestionUsuarios.a�adir(usu);
		
		Usuario usu2 = new Usuario(24,"fulanita");
		gestionUsuarios.a�adir(usu2);
		
		Loco joker = new Loco();
		joker.setNombre("El bromas");
		joker.setTipoLocura(true);
		System.out.println("Joker: "+ joker.getNombre());
		if(joker.isTipoLocura()) {
			System.out.println("Esta loco, lo a�ado a la lista de usuarios");
			gestionUsuarios.a�adir(joker);
		}
		else {
			System.out.println("Esta cuerdo");
		}
		gestionUsuarios.listar();
		//gestionUsuarios.listarUsuario("El bromas");
		System.out.println("Cambio edad y nombre a el bromas");
		gestionUsuarios.modificarAmbas("El bromas","El Joker", 23);
		gestionUsuarios.listar();
		gestionUsuarios.listarEdad(2);
		gestionUsuarios.listarEdadRango(21,24);
		gestionUsuarios.eliminarUsuario("El Joker");
		gestionUsuarios.listar();
		EjemploHashMap.probandoHashMap();
		gestionUsuarios.eliminarTodo();
		System.out.println("Elimino todo");
		gestionUsuarios.listar();
		*/
		
		//ProbandoVehiculos.probar();
		
		
		//PruebaMemoria.pruebaPasoReferencia();
		
		EjemploLambdas.ejecutarLambdas();
	}

}